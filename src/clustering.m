function clustering( feats_mat_path )
    
    fprintf('loading feats...');
    load(feats_mat_path, 'feats_data');
    fprintf('done.\n');
    
    feats = zeros(length(feats_data), length(feats_data{1}.feat_vector));
    for i=1:length(feats_data)
    	feat_cur = feats_data{i}.feat_vector;
        feats(i, :) = feat_cur;
    end
    
    Y = pdist(feats);
    Z = linkage(Y);
    
    T = cluster(Z, 'cutoff', 1.0);
    
    unique_T = unique(T);
    number_of_persons = length(unique_T);    
    for i=1:length(unique_T)
        cl_id = unique_T(i);
        cur_ids = find(T == cl_id)';
        fprintf('person #%d:', cl_id);
        for k=cur_ids
            img_name = feats_data{k}.img_name;
            fprintf(' %s', img_name);
        end
        fprintf('\n');
    end
    fprintf('number of different persons: %d\n', number_of_persons);
end

