function visualize(data_dir, output_dir)
    
    if ~exist(output_dir, 'dir')
        mkdir(output_dir);
    end    
    
    imgs_files = dir(fullfile(data_dir, '*.tif'));
    imgs_names = {imgs_files.name};
    
    for k=1:length(imgs_names)
        
        fprintf('processing image [%d/%d]\n', k, length(imgs_names));
        
        img_name = imgs_names{k};
        img_path = fullfile(data_dir, img_name);
        data = hand_detection(img_path, false);
        
        img = imread(img_path);
        h = imshow(img);
        hold on;
        plot(data.x, data.y);
        plot(data.x_hull, data.y_hull);

        for i=1:length(data.defects)
            defect = data.defects{i};

            pt_id = defect(3);
            pt_id_st = defect(1);
            pt_id_en = defect(2);

            pt = data.contour{pt_id};
            pt_st = data.contour{pt_id_st};
            pt_en = data.contour{pt_id_en};

            plot(pt(1), pt(2), 'g*');
            plot(pt_st(1), pt_st(2), 'r*');
            plot(pt_en(1), pt_en(2), 'r*');
            text(pt(1), pt(2), num2str(i), 'Color', [1 1 0]);
        end
        if data.is_right_hand
            text(10, 10, 'right hand', 'Color', [1 1 0]);
        else
            text(10, 10, 'left hand', 'Color', [1 1 0]);
        end
        
        output_path = fullfile(output_dir, img_name);
        saveas(h, output_path);
    end
end

