function bw = smart_binarization(img_gray, level)
    
    img_mean = mean(img_gray(:));
    img_min = min(img_gray(:));
    thr = img_min + level * (img_mean - img_min);
    
    bw = img_gray > thr;    
    bw = imfill(bw, 'holes');
    
    se = strel('disk', 3);
    bw = imopen(bw, se);

end