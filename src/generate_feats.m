function generate_feats(data_dir)
    
    imgs_files = dir(fullfile(data_dir, '*.tif'));
    imgs_names = {imgs_files.name};
    good_mask = false(1, length(imgs_names));
    feats_data = cell(1, length(imgs_names));
    
    for k=1:length(imgs_names)
        
        fprintf('processing image [%d/%d]...', k, length(imgs_names));
        
        img_name = imgs_names{k};
        img_path = fullfile(data_dir, img_name);
        data = hand_detection(img_path, false);
        
        if length(data.defects) ~= 4
            fprintf('bad.\n');
            continue;
        end
        good_mask(k) = true;        
        feat_data = struct();
        feat_data.img_name = img_name;
        
        feat_vector = zeros(1, 8);
        norm_factor = 1;
        for i=1:length(data.defects)
            defect = data.defects{i};

            pt_id = defect(3);
            pt_id_st = defect(1);
            pt_id_en = defect(2);

            pt = data.contour{pt_id};
            pt_st = data.contour{pt_id_st};
            pt_en = data.contour{pt_id_en};
            
            norm_a = norm(pt - pt_st);
            norm_b = norm(pt - pt_en);
            
            if i == 1
                norm_factor = norm_b;
            end
            
            feat_vector(2 * i - 1) = norm_a;
            feat_vector(2 * i) = norm_b;
        end
        feat_data.feat_vector = feat_vector ./ norm_factor;
        feats_data{k} = feat_data;
        fprintf('good.\n');
    end
    
    k_good = nnz(good_mask);
    k_bad = nnz(~good_mask);
    
    fprintf('total number of images: %d\n', length(imgs_names));
    fprintf('number of bad images: %d\n', k_bad);
    fprintf('number of good images: %d\n', k_good);
    
    feats_data = feats_data(good_mask);
    save('feats_data.mat', 'feats_data');
end

