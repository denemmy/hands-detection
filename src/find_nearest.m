function find_nearest( index, data_dir, feats_mat_path )
    
    K = 3;
    fprintf('loading feats...');
    load(feats_mat_path, 'feats_data');
    fprintf('done.\n');
    
    feat_data = feats_data{index};
    feats_data(index) = [];
    feat_ref = feat_data.feat_vector;
    img_name_ref = feat_data.img_name;
    img_path_ref = fullfile(data_dir, img_name_ref);
    
    distance_vals = zeros(1, length(feats_data));
    for i=1:length(feats_data)
    	feat_cur = feats_data{i}.feat_vector;
        dist_val = norm(feat_cur - feat_ref);
        distance_vals(i) = dist_val;
    end
    
    [~, sort_idx] = sort(distance_vals);
    
    sort_idx = sort_idx(1:K);
    
    figure, imshow(imread(img_path_ref));
    for i=1:length(sort_idx)
        idx = sort_idx(i);
        img_name = feats_data{idx}.img_name;
        img_path = fullfile(data_dir, img_name);
        figure, imshow(imread(img_path));
    end

end

