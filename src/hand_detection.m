function data = hand_detection(img_path, verbose)
    verbose_details = false;
    addpath('../lib');

    %% read image
    img = imread(img_path);
    img = im2double(img);
    if verbose_details
        figure, imshow(img);
    end
    %% preprocessing
    img = imgaussfilt(img, 0.3);
    
    if size(img, 3) == 3
        img_gray = rgb2gray(img);
    else
        img_gray = img;
    end
    if verbose_details
        figure, imshow(img_gray, []);
    end
    
    %% mark foreground    
    bw = smart_binarization(img_gray, 0.8);
    if verbose_details
        figure, imshow(bw), title('bw');
    end
    
    %% segmentation
    stats = regionprops(bw, 'Area', 'ConvexHull', 'BoundingBox', 'ConvexImage', 'Image', 'PixelIdxList');
    [~, max_id] = max([stats.Area]);
    properties = stats(max_id);
    
    bbox = properties.BoundingBox;
    bbox_w = min(bbox(3:4));
    
    bw_region = false(size(bw));
    bw_region(properties.PixelIdxList) = true;
    
    convex_hull = properties.ConvexHull;    
    % convex_bw = roipoly(size(bw, 1), size(bw, 2), convex_hull(:, 1), convex_hull(:, 2));
    
    %% find contour and convex hull with opencv
    contours = cv.findContours(bw_region, 'Method', 'None');
    contour = contours{1};
    convexhull = cv.convexHull(contour, 'ReturnPoints', false);
    convexhull = sort(convexhull, 'ascend');
    defects = cv.convexityDefects(contour, convexhull);
    
    for i=1:length(defects)
        defect = defects{i};
        defect(1) = min(defect(1) + 1, length(contour));
        defect(2) = min(defect(2) + 1, length(contour));
        defect(3) = min(defect(3) + 1, length(contour));
        defects{i} = defect;
    end
        
    x = zeros(1, length(contour));
    y = zeros(1, length(contour));
    for i=1:length(contour)
    	pt = contour{i};
        x(i) = pt(1);
        y(i) = pt(2);
    end
    
    x_hull = zeros(1, length(convexhull));
    y_hull = zeros(1, length(convexhull));
    for i=1:length(convexhull)
       pt = contour{convexhull(i) + 1};
       x_hull(i) = pt(1);
       y_hull(i) = pt(2);
    end
    
    if verbose_details
        figure, imshow(img);
        hold on;
        plot(x, y);
        plot(x_hull, y_hull); 

        for i=1:length(defects)
            defect = defects{i};

            pt_id = defect(3);
            pt_id_st = defect(1);
            pt_id_en = defect(2);

            pt = contour{pt_id};
            pt_st = contour{pt_id_st};
            pt_en = contour{pt_id_en};

            plot(pt(1), pt(2), 'g*');
            plot(pt_st(1), pt_st(2), 'r*');
            plot(pt_en(1), pt_en(2), 'r*');
        end
    end
    
    %% filter points
    defects = filter_defects(contour, defects, bbox_w);
    if verbose_details
        figure, imshow(img);
        hold on;
        plot(x, y);
        plot(x_hull, y_hull); 

        for i=1:length(defects)
            defect = defects{i};

            pt_id = defect(3);
            pt_id_st = defect(1);
            pt_id_en = defect(2);

            pt = contour{pt_id};
            pt_st = contour{pt_id_st};
            pt_en = contour{pt_id_en};

            plot(pt(1), pt(2), 'g*');
            plot(pt_st(1), pt_st(2), 'r*');
            plot(pt_en(1), pt_en(2), 'r*');
            text(pt_st(1), pt_st(2), num2str(i));
            text(pt_en(1), pt_en(2), num2str(i));
        end
    end
    %% join points
    
    defects = join_points(contour, defects);
    if verbose_details
        figure, imshow(img);
        hold on;
        plot(x, y);
        plot(x_hull, y_hull); 

        for i=1:length(defects)
            defect = defects{i};

            pt_id = defect(3);
            pt_id_st = defect(1);
            pt_id_en = defect(2);

            pt = contour{pt_id};
            pt_st = contour{pt_id_st};
            pt_en = contour{pt_id_en};

            plot(pt(1), pt(2), 'g*');
            plot(pt_st(1), pt_st(2), 'r*');
            plot(pt_en(1), pt_en(2), 'r*');
            text(pt_st(1), pt_st(2), num2str(i));
            text(pt_en(1), pt_en(2), num2str(i));
        end
    end
    [defects_sorted, is_right_hand] = sort_defects(contour, defects);
    
    data = struct();
    data.x = x;
    data.y = y;
    data.x_hull = x_hull;
    data.y_hull = y_hull;
    data.defects = defects_sorted;
    data.is_right_hand = is_right_hand;
    data.contour = contour;
    
    if verbose
        figure, imshow(img);
        hold on;

        for i=1:length(defects_sorted)
            defect = defects_sorted{i};

            pt_id = defect(3);
            pt_id_st = defect(1);
            pt_id_en = defect(2);

            pt = contour{pt_id};
            pt_st = contour{pt_id_st};
            pt_en = contour{pt_id_en};

            plot(pt(1), pt(2), 'g*');
            plot(pt_st(1), pt_st(2), 'r*');
            plot(pt_en(1), pt_en(2), 'r*');
            
            plot([pt_st(1), pt(1)], [pt_st(2), pt(2)], 'g');
            plot([pt(1), pt_en(1)], [pt(2), pt_en(2)], 'g');
            
            text(pt(1), pt(2), num2str(i), 'Color', [1 1 0]);
        end
        if is_right_hand
            text(10, 10, 'right hand', 'Color', [1 1 0]);
        else
            text(10, 10, 'left hand', 'Color', [1 1 0]);
        end
    end
end

function [defects_sorted, is_right_hand] = sort_defects(contour, defects)
    sel_i = -1;
    max_len = -1;
    is_right_hand = true;
    for i=1:length(defects)
        defect = defects{i};
        pt_id = defect(3);
        pt_id_st = defect(1);
        pt_id_en = defect(2);
        
        pt = contour{pt_id};
        pt_st = contour{pt_id_st};
        pt_en = contour{pt_id_en};
        
        len_1 = norm(pt - pt_st);
        len_2 = norm(pt - pt_en);
        max_cur_len = max(len_1, len_2);
        if max_cur_len > max_len
        	sel_i = i;
            if len_1 < len_2
                is_right_hand = true;
            else
                is_right_hand = false;
            end
            max_len = max_cur_len;
        end
    end
    if sel_i > 0
        sorted_idx = [sel_i:length(defects) 1:sel_i-1];
        defects_sorted = defects(sorted_idx);
    else
        defects_sorted = defects;
    end
end

function defects = join_points(contour, defects)

    % calculate perimiter
    contour_dist = zeros(1, length(contour));
    for i=2:length(contour)
        pt_prev = contour{i-1};
        pt = contour{i};
        dist_value = norm(pt - pt_prev);
        contour_dist(i) = dist_value;
    end
    pt_prev = contour{end};
    pt = contour{1};
    dist_value = norm(pt - pt_prev);
    contour_dist(1) = dist_value;
    
    fingers_dist = zeros(1, length(defects));
    for i=1:length(defects)
    	defect = defects{i};
        
        pt_id = defect(3);
        pt_id_st = defect(1);
        pt_id_en = defect(2);
        
        pt = contour{pt_id};
        pt_st = contour{pt_id_st};
        pt_en = contour{pt_id_en};
        
        dist_value = (norm(pt_st - pt) + norm(pt_en - pt)) / 2;
        fingers_dist(i) = dist_value;
    end
    fingers_mean = mean(fingers_dist);
    
    min_dist_between = 0.7 * fingers_mean;
    max_idx = length(contour);
    for i=1:length(defects)
        defect = defects{i};
        for j=i+1:length(defects)
            defect_next = defects{j};
            
            pt_id_st = defect(1);
            pt_id_en = defect(2);
            
            pt_id_st_next = defect_next(1);
            pt_id_en_next = defect_next(2);
            
            bigger_id_1 = max(pt_id_en, pt_id_st_next);
            smaller_id_1 = min(pt_id_en, pt_id_st_next);
            
            straight_idx_1 = smaller_id_1:bigger_id_1;
            reverse_idx_1 = [bigger_id_1:max_idx, 1:smaller_id_1];
            dist_between_1_1 = sum(contour_dist(straight_idx_1));
            dist_between_1_2 = sum(contour_dist(reverse_idx_1));
            
            bigger_id_2 = max(pt_id_en_next, pt_id_st);
            smaller_id_2 = min(pt_id_en_next, pt_id_st);
            
            straight_idx_2 = smaller_id_2:bigger_id_2;
            reverse_idx_2 = [bigger_id_2:max_idx, 1:smaller_id_2];
            
            dist_between_2_1 = sum(contour_dist(straight_idx_2));
            dist_between_2_2 = sum(contour_dist(reverse_idx_2));

            do_change = false;
            if dist_between_1_1 < min_dist_between
                % join
                pt_id = straight_idx_1(ceil(end/2));
                defect_next(1) = pt_id;
                defect(2) = pt_id;
                do_change = true;
            elseif dist_between_1_2 < min_dist_between
                % join
                pt_id = reverse_idx_1(ceil(end/2));
                defect_next(1) = pt_id;
                defect(2) = pt_id;
                do_change = true;
            end
            
            if dist_between_2_1 < min_dist_between
                % join
                pt_id = straight_idx_2(ceil(end/2));
                defect_next(2) = pt_id;
                defect(1) = pt_id;
                do_change = true;
            elseif dist_between_2_2 < min_dist_between
                % join
                pt_id = reverse_idx_2(ceil(end/2));
                defect_next(2) = pt_id;
                defect(1) = pt_id;
                do_change = true;
            end
            
            if do_change
            	defects{i} = defect;
                defects{j} = defect_next;
            end
        end
    end

end

function defects_filt = filter_defects(contour, defects, bbox_w)
    len_thres = 0.2 * bbox_w;
    ratio_thres = 2.3;
    angle_thres = 100;
    
    sel_mask = false(1, length(defects));
    for i=1:length(defects)
    	defect = defects{i};
        
        pt_id = defect(3);
        pt_id_st = defect(1);
        pt_id_en = defect(2);
        
        % bug fix in convexityDefects function
        if pt_id_st > pt_id_en
            continue            
        end
        
        pt = contour{pt_id};
        pt_st = contour{pt_id_st};
        pt_en = contour{pt_id_en};        
        
        a = pt - pt_st;
        b = pt - pt_en;
        
        % calculate distances
        norm_a = norm(a);
        norm_b = norm(b);
        
        % calculate ratio
        ratio_v = max(norm_a, norm_b) / min(norm_a, norm_b);
        
        % calculate angle
        cos_angle = sum(a .* b) / norm_a / norm_b;
        angle = acos(cos_angle) * 180 / pi;
        angle = real(angle);
        
        is_angle = angle < angle_thres;
        is_len = norm_a > len_thres && norm_b > len_thres;
        is_ratio = ratio_v < ratio_thres;
        if is_angle && is_len && is_ratio
            sel_mask(i) = true;
        end
    end
    defects_filt = defects(sel_mask);
end

function pts = find_points(contour)

    K = 150;
    angle_thres = 60;
    
    start_id = 1 + K;
    end_id = length(contour) - K;
    
    sel_idx = [];
    angles = zeros(1, end_id - start_id + 1);
    
    for i=start_id:end_id
        pt_q = contour{i};
        pt_prev = contour{i-K};
        pt_next = contour{i+K};
        
        a = pt_prev - pt_q;
        b = pt_next - pt_q;
        
        cos_angle = sum(a .* b) / (norm(a) * norm(b));
        cos_angle = min(cos_angle, 1-eps);
        angle = real(acosd(cos_angle));
        angles(i-K) = angle;
        if angle < angle_thres
        	sel_idx = [sel_idx, i];
        end
        fprintf('angle: %f\n', angle);
    end
    
    x = zeros(1, length(contour));
    y = zeros(1, length(contour));
    for i=1:length(contour)
        pt = contour{i};
        x(i) = pt(1);
        y(i) = pt(2);
    end
    
    figure, plot(x, y, 'b.');
    hold on;
    pt_st = 2075;    
    plot(x(pt_st-K:pt_st+K), y(pt_st-K:pt_st+K), 'r.');
    plot(x(pt_st), y(pt_st), 'g.');
    
    figure, plot(1:length(angles), angles);
    
    values = 180 - angles;
    [~, pt_ids]= findpeaks(values, 'MinPeakProminence', 60);
    
    figure, plot(x, y, 'b.');
    x_found = x(pt_ids);
    y_found = y(pt_ids);
    hold on;
    plot(x_found, y_found, 'g*');    
    
end

